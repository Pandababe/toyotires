<?php
	/*
		Template Name: Dealer
	*/
?>

<style>
	#map {
		width: 100%;
		height: 700px;
	}
</style>
<body>

	<?php
		echo 'Dealers List';
		echo '<br>';
		$dealers_list = get_field('dealers_information');
		print_r($dealers_list);
		echo '<br>';
		echo '<br>';
	?>

	<div id="map"></div>
	<button id="get">Get Direction</button>

	<script>
      var map;
      function initMap(lat, lng) {
		myLatLng = {
			lat: 14.8503716,
			lng: 120.9995984
		}
        map = new google.maps.Map(document.getElementById('map'), {
         	center: myLatLng,
          	zoom: 16
		});
		var marker = new google.maps.Marker({
          	position: myLatLng,
         	map: map,
          	title: 'Hello World!'
        });
	  }
	  document.getElementById('get').onclick = () => {
				alert('test');
	  }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCT8Wl4HH96KM8ad7rVHsIyJukypYHEoWw&callback=initMap" async defer></script>


</body>
</html>