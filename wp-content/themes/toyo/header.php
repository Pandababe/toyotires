<?php 
    echo '<h1>'. the_title() . '</h1>';
    echo 'Site Icon';
    echo '<br>';
    echo get_site_icon_url();
    echo '<br>';
    echo '<br>';
    echo 'Logo';
    echo '<br>';
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
    echo $image[0];
    echo '<br>';
    echo '<br>';
    echo 'Primary Menu';
    echo '<br>';
    wp_nav_menu( array(
        'menu'           => 'Primary Menu',
        'theme-location' => 'primary-menu',
        'menu_class'     => 'custom-class'
    ) );
    echo '<br>';
    echo 'Secondary Menu';
    echo '<br>';
    wp_nav_menu( array(
        'menu'           => 'Secondary Menu',
        'theme-location' => 'secondary-menu',
        'menu_class'     => 'custom-class'
    ) );
    echo '<br>';
?>